import Vue from 'vue'
import Router from 'vue-router'
import siteMap from '@/components/siteMap'
import detail from '@/components/detail'
import feedback from '@/components/feedback'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: siteMap,
      name: 'siteMap'
    },
    {
      path: '/detail',
      component: detail,
      name: 'detail'
    },
    {
      path: '/feedback',
      component: feedback,
      name: 'feedback'
    }
  ]
})
